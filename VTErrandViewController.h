//
// Created by Gerard Moreno-Torres Bertran on 26/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VTErrand;


@interface VTErrandViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

- (void)setErrand:(VTErrand *)errand;

@end