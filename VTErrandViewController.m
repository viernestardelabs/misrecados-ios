//
// Created by Gerard Moreno-Torres Bertran on 26/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "VTErrandViewController.h"
#import "VTErrand.h"
#import "VTCompany.h"
#import "NSDate+VTDateExtension.h"
#import "NSNumber+VTNumericExtensions.h"
#import "VTMessageCell.h"
#import "VTMessage.h"

@interface VTErrandViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *phone;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) VTErrand *errand;
@property (strong, nonatomic) NSMutableArray *messages;


@end

@implementation VTErrandViewController

- (void) viewDidLoad {

}

- (void) setErrand:(VTErrand *)errand {
    self.view.backgroundColor = [UIColor colorWithWhite:0.941 alpha:1.000];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    self.messages = [[NSMutableArray alloc] init];
    self.title = errand.errandName;
    _errand = errand;
    _companyName.text = errand.company.name;
    _date.text = errand.date.toUIString;
    _price.text = errand.price.toPriceString;
    _address.text = errand.company.address;
    _phone.text = errand.company.phone;
    [_map setRegion:errand.getMapRegion animated:YES];
    [self setMapPin:errand];
    [self loadMessages];
}

- (void)viewDidLayoutSubviews {
    [_scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
}

- (void) loadMessages {
    VTMessage *message = [[VTMessage alloc] initWithContent:@"Mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba, mensaje de prueba"];
    [self addMessage:message];
    [self addMessage:message];
    [self addMessage:message];
    [self addMessage:message];
    [_tableView reloadData];
}

- (void)setMapPin:(VTErrand *)errand {
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:_errand.company.location.coordinate];
    annotation.title = _errand.company.name;
    [_map addAnnotation:annotation];
}

- (void) addMessage:(VTMessage *)message {
    [_messages addObject:message];
    _scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, self.tableViewHeight + _scrollView.frame.origin.y);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _messages.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VTMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessageCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    [cell setMessage:[_messages objectAtIndex:indexPath.row]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self tableView:tableView cellForRowAtIndexPath:indexPath].frame.size.height;
}

- (CGFloat)tableViewHeight {
    CGFloat totalHeight = 0;

    for (int i = 0; i < [self tableView:nil numberOfRowsInSection:0]; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        totalHeight += [self tableView:nil heightForRowAtIndexPath:indexPath];
    }

    return totalHeight;
}

@end