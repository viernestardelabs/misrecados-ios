//
// Created by Gerard Moreno-Torres Bertran on 26/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "VTErrand.h"
#import "VTCompany.h"

@interface  VTErrand()

@property (nonatomic) NSString *errandName;
@property (nonatomic) VTCompany *company;
@property (nonatomic) NSDate *date;
@property (nonatomic) NSNumber *price;
@property (nonatomic) BOOL chatAvailable;

@end

@implementation VTErrand {

}
- (instancetype)initWithErrandName:(NSString *)errandName
                           company:(VTCompany *)company
                              date:(NSDate *)date
                             price:(NSNumber *)price
                     chatAvailable:(BOOL)chatAvailable {
    self = [super init];
    if (self) {
        self.errandName = errandName;
        self.company = company;
        self.date = date;
        self.price = price;
        self.chatAvailable = chatAvailable;
    }

    return self;
}

- (NSString *)errandName {
    return _errandName;
}

- (VTCompany *)company {
    return _company;
}

- (NSDate *)date {
    return _date;
}

- (NSNumber *)price {
    return _price;
}

- (BOOL)chatIsAvailable {
    return _chatAvailable;
}

- (MKCoordinateRegion)getMapRegion {
    return MKCoordinateRegionMake(self.company.location.coordinate, MKCoordinateSpanMake(0.008, 0.008));
}

@end