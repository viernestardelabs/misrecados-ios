//
//  UIPaddedLabel.m
//  Karma
//
//  Created by Gerard Moreno-Torres Bertran on 25/06/14.
//  Copyright (c) 2014 Biznagames. All rights reserved.
//

#import "UIPaddedLabel.h"

@implementation UIPaddedLabel

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, _edgeInsets)];
}

@end
