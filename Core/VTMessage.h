//
// Created by Gerard Moreno-Torres Bertran on 27/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VTSubject;


@interface VTMessage : NSObject

@property (strong, nonatomic) NSDate *dateSent;
@property (strong, nonatomic) NSDate *dateReceived;
@property (strong, nonatomic) VTSubject *sender;
@property (strong, nonatomic) VTSubject *receiver;

- (instancetype)initWithContent:(NSString *)content;

- (NSString *)text;

@end