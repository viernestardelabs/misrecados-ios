//
// Created by Gerard Moreno-Torres Bertran on 27/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import "VTSubject.h"
#import "VTMessage.h"

@interface VTSubject()

@property (copy, nonatomic) NSString *id;

@end

@implementation VTSubject

- (instancetype)initWithId:(NSString *)id {
    self = [super init];
    if (self) {
        self.id = id;
    }

    return self;
}

- (NSString *)id {
    return _id;
}

- (void)sendMessage:(VTMessage *)message {
    //TODO: implement message sending process
}

@end