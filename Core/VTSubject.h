//
// Created by Gerard Moreno-Torres Bertran on 27/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VTMessage;


@interface VTSubject : NSObject
- (instancetype)initWithId:(NSString *)id;


- (NSString *) id;
- (void) sendMessage:(VTMessage *)message;

@end