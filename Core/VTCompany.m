//
// Created by Gerard Moreno-Torres Bertran on 26/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "VTCompany.h"

@interface VTCompany()

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *phone;
@property (nonatomic) NSString *email;
@property (nonatomic) CLLocation *location;

@end

@implementation VTCompany {

}

- (instancetype)initWithName:(NSString *)name
                     address:(NSString *)address
                       phone:(NSString *)phone
                       email:(NSString *)email
                    location:(CLLocation *)location {
    self = [super init];
    if (self) {
        self.name = name;
        self.address = address;
        self.phone = phone;
        self.email = email;
        self.location=location;
    }

    return self;
}

- (NSString *)name {
    return _name;
}

- (NSString *)address {
    return _address;
}

- (NSString *)phone {
    return _phone;
}

- (NSString *)email {
    return _email;
}

- (CLLocation *)location {
    return _location;
}


@end