//
// Created by Gerard Moreno-Torres Bertran on 27/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import "VTMessage.h"
#import "VTSubject.h"

@interface VTMessage()

@property (copy, nonatomic) NSString *content;

@end

@implementation VTMessage {

}

- (instancetype)initWithContent:(NSString *)content {
    self = [super init];
    if (self) {
        self.content = content;
    }
    return self;
}

- (NSString *)text {
    return _content;
}

@end