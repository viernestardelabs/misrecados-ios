//
// Created by Gerard Moreno-Torres Bertran on 26/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class CLLocation;
@class VTCompany;


@interface VTErrand : NSObject

- (instancetype)initWithErrandName:(NSString *)errandName
                           company:(VTCompany *)company
                              date:(NSDate *)date
                             price:(NSNumber *)price
                     chatAvailable:(BOOL)chatAvailable;

- (NSString *)errandName;
- (VTCompany *)company;
- (NSDate *)date;
- (NSNumber *)price;
- (BOOL) chatIsAvailable;
- (MKCoordinateRegion)getMapRegion;

@end