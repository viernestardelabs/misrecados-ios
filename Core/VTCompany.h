//
// Created by Gerard Moreno-Torres Bertran on 26/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VTSubject.h"

@class CLLocation;

@interface VTCompany : VTSubject

- (instancetype)initWithName:(NSString *)name
                     address:(NSString *)address
                       phone:(NSString *)phone
                       email:(NSString *)email
                    location:(CLLocation *)location;

- (NSString *) name;
- (NSString *) address;
- (NSString *) phone;
- (NSString *) email;
- (CLLocation *) location;

@end