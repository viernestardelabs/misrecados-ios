//
//  VTMainViewController.h
//  MisRecados
//
//  Created by Gerard Moreno-Torres Bertran on 22/08/14.
//  Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTMainViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
