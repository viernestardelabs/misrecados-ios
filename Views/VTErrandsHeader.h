//
//  VTErrandsHeader.h
//  MisRecados
//
//  Created by Gerard Moreno-Torres Bertran on 26/08/14.
//  Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTErrandsHeader : UITableViewCell

@property (weak,nonatomic) IBOutlet UILabel *numberOfErrands;
@property (weak,nonatomic) IBOutlet UILabel *title;

@end
