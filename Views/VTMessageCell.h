//
//  VTMessageCell.h
//  MisRecados
//
//  Created by Gerard Moreno-Torres Bertran on 28/08/14.
//  Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VTMessage;

@interface VTMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UIImageView *image;

@end
