//
// Created by Gerard Moreno-Torres Bertran on 22/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import "VTErrandCell.h"
#import "VTErrand.h"
#import "VTCompany.h"
#import "NSDate+VTDateExtension.h"
#import "NSNumber+VTNumericExtensions.h"


@interface VTErrandCell ()

@property (weak, nonatomic) IBOutlet UILabel *errandName;
@property (weak, nonatomic) IBOutlet UILabel *companyName;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIImageView *chatAvailable;
@property (nonatomic) VTErrand *errand;

@end

@implementation VTErrandCell {

}

- (VTErrand *)getErrand {
    return _errand;
}

- (void)setErrand:(VTErrand *)errand {
    _errand = errand;
    _errandName.text = errand.errandName;
    _companyName.text = errand.company.name;
    _date.text = errand.date.toUIString;
    _price.text = errand.price.toPriceString;
    if (!errand.chatIsAvailable) _chatAvailable.image = nil;
}

- (NSString *) getPriceString:(double)numericPrice {
    NSString *price = [NSString stringWithFormat:@"%.1f", numericPrice];
    NSString *coin = @"€";
    NSMutableString *mutableString= [[NSMutableString alloc] init];
    [mutableString appendString:price];
    [mutableString appendString:coin];
    return mutableString.description;
}

@end