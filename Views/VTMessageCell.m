//
//  VTMessageCell.m
//  MisRecados
//
//  Created by Gerard Moreno-Torres Bertran on 28/08/14.
//  Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import "VTMessageCell.h"
#import "UIPaddedLabel.h"
#import "VTMessage.h"

@implementation VTMessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor orangeColor];
        _content.numberOfLines = 0;
        _content.adjustsFontSizeToFitWidth = NO;
        _content.lineBreakMode= NSLineBreakByWordWrapping;
    }
    return self;
}

- (void)adjustHeight {
    [_content sizeToFit];
    CGRect rect = CGRectMake(_content.frame.origin.x, _content.frame.origin.y, _content.frame.size.width, _content.frame.size.height + 20);
    _content.frame = rect;
    self.frame = rect;
}

- (CGFloat)getHeight {
    return _content.frame.size.height;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
