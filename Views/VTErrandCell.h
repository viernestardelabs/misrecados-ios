//
// Created by Gerard Moreno-Torres Bertran on 22/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VTErrand;


@interface VTErrandCell : UITableViewCell

- (VTErrand *)getErrand;
- (void) setErrand:(VTErrand*)errand;

@end