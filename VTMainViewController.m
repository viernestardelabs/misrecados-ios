//
//  VTMainViewController.m
//  MisRecados
//
//  Created by Gerard Moreno-Torres Bertran on 22/08/14.
//  Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "VTMainViewController.h"
#import "VTErrandCell.h"
#import "SmartStoryboard.h"
#import "VTErrand.h"
#import "VTCompany.h"
#import "VTErrandsHeader.h"
#import "VTErrandViewController.h"
#import "NSNumber+VTNumericExtensions.h"

@interface VTMainViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation VTMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
	// Do any additional setup after loading the view, typically from a nib.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_tableView != nil)
        _tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    return 4;
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return @"Pendientes";
    else if (section == 1)
        return  @"Realizados";
    else
        return @"Otros";
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    VTErrandsHeader *header = [tableView dequeueReusableCellWithIdentifier:@"Header"];

    if (section == 0)
        header.title.text = @"Pendientes";
    else if (section == 1)
        header.title.text = @"Realizados";

    NSMutableString *mutableString = [[NSMutableString alloc] init];
    NSInteger numberOfRows = [self tableView:tableView numberOfRowsInSection:section];
    [mutableString appendString: [NSString stringWithFormat:@"%ld", (long)numberOfRows]];
    [mutableString appendString:@" recados"];
    header.numberOfErrands.text = mutableString.description;

    return (UIView *)header;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VTErrandCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PatoCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    VTCompany *company = [[VTCompany alloc] initWithName:@"Copicentro"
                                                 address:@"Avenida San Isidro nº2"
                                                   phone:@"692944828"
                                                   email:@"germtf@gmail.com"
                                                location:[[CLLocation alloc] initWithLatitude:37.200 longitude:-4.65]];
    VTErrand *errand = [[VTErrand alloc] initWithErrandName:@"Fotocopias"
                                                    company:company
                                                       date:[VTMainViewController dateWithYear:2014 month:10 day:30]
                                                      price:@15.5F
                                              chatAvailable:NO];
    [cell setErrand:errand];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VTErrandViewController *errandViewController = (VTErrandViewController *)[SmartStoryboard instantiateViewControllerWithIdentifier:@"ErrandView"];
    VTErrandCell *cell = (VTErrandCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    [errandViewController setErrand:cell.getErrand];
    [self.navigationController pushViewController:errandViewController animated:YES];
}

+ (NSDate *)dateWithYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    return [calendar dateFromComponents:components];
}

@end
