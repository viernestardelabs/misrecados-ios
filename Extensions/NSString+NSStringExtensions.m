//
// Created by Gerard Moreno-Torres Bertran on 27/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import "NSString+NSStringExtensions.h"


@implementation NSString (NSStringExtensions)

- (NSString *)concatWith:(NSString *)string {
    NSMutableString *mutableString = [[NSMutableString alloc] init];
    [mutableString appendString:self];
    [mutableString appendString:string];
    return mutableString;
}

@end