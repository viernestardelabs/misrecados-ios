//
// Created by Gerard Moreno-Torres Bertran on 27/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import "NSDate+VTDateExtension.h"


@implementation NSDate (VTDateExtension)

- (NSString *)toUIString {
    static NSDateFormatter *formatter = nil;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm - dd/MM/yyyy"];
    }
    return [formatter stringFromDate:self];
}

- (NSString *)toDBString {
    static NSDateFormatter *formatter = nil;
    if (formatter == nil) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy/MM/dd, HH:mm"];
    }
    return [formatter stringFromDate:self];
}


@end