//
// Created by Gerard Moreno-Torres Bertran on 27/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import "NSNumber+VTNumericExtensions.h"
#import "NSString+NSStringExtensions.h"


@implementation NSNumber (VTNumericExtensions)

- (NSString *)toString {
    return [NSString stringWithFormat:@"%.1f", self.doubleValue];
}

- (NSString *)toPriceString {
    return [self.toString concatWith:@"€"];
}

@end