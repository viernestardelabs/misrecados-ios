//
// Created by Gerard Moreno-Torres Bertran on 27/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (VTDateExtension)

- (NSString *) toUIString;
- (NSString *) toDBString;

@end