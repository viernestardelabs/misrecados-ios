//
// Created by Gerard Moreno-Torres Bertran on 22/08/14.
// Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import "SmartStoryboard.h"


@implementation SmartStoryboard {

}

+ (UIViewController*) instantiateViewControllerWithIdentifier:(NSString*) identifier
{
    return [[SmartStoryboard getStoryboard] instantiateViewControllerWithIdentifier:identifier];
}

+ (UIStoryboard*) getStoryboard
{
    UIStoryboard* storyboard;

    if (self)
    {
        storyboard = ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) ?
                [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil] :
                [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }

    storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];   //TODO: TESTING ONLY, DELETE THIS LINE FOR RELEASE VERSION
    return storyboard;
}


@end