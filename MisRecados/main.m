//
//  main.m
//  MisRecados
//
//  Created by Gerard Moreno-Torres Bertran on 22/08/14.
//  Copyright (c) 2014 ViernesTardesLab. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VTAppDelegate class]));
    }
}
